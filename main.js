const assets = require("assets");
const assetColors = assets.colors.get();
const assetCharacterStyles = assets.characterStyles.get();
const fs = require("uxp").storage.localFileSystem;

function exportAssetsToJson() {
    let exportJSON = {};

    exportJSON.colors = assetColors.map(color => {
        color.color = color.color.toRgba();
        return color;
    });

    exportJSON.characterStyles = assetCharacterStyles.map(characterStyle => {
        characterStyle.style.fill = characterStyle.style.fill.toRgba();
        return characterStyle;
    });

    fs.getFolder().then(folder => {
        folder.createFile("export.json", { overwrite: true }).then(file => {
            file.write(JSON.stringify(exportJSON));
        });
    });
}

module.exports = {
    commands: {
        exportAssetsToJson
    }
};
